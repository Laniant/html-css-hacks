**1. Responsive navigation bar without JS or lists.**

Uses <span> and display: block to essentially do what a list would do.\
Can easily be adapted to fit most situations.\
Using role="navigation" avoids and footer interference.